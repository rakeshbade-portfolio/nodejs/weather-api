//Express
const express = require('express')
const OAuth = require('oauth')
const app = express()

//Default Port

const port = process.env.PORT || 4545

const yahoo_customerKey = process.env.YAHOO_CKEY;
const yahoo_customerSecret = process.env.YAHOO_CSECRET;
const yahoo_appId = process.env.YAHOO_APPID;

//INIT Yahoo End point
const yahoo_api_url = "https://weather-ydn-yql.media.yahoo.com/forecastrss"

//Teleport Cities End point
const teleport_api_url = "https://api.teleport.org/api/cities/"

//Access Controls
app.use((req, res, next) => {
    res.append('Access-Control-Allow-Origin', ['*'])
    res.append('Access-Control-Allow-Methods', 'GET')
    res.append('Access-Control-Allow-Headers', 'Content-Type')
    res.append('Content-Type', 'application/json')
    next()
});

app.get('/', (req, res) => res.send());

//base url
app.get('/weather', (req, res) => {
    //init params
    var fields = ['city'];

    //verify all params
    if (findParams(req.query, fields) !== true) {
        res.status(404);
        return res.json({ message: "Reqired fields", error: fields });
    }

    //get weather based on city
    getWeatherDetails(req.query.city).then((data) => {
        var tempInfo = typeof data == 'object' ? data : JSON.parse(data);
        res.status(200)
        return res.json(tempInfo);
    }).catch((err) => {
        res.status(500);
        return res.json(err);
    })
})

app.get('/cities', (req, res) => {
    //init params
    var fields = ['search'];

    //verify all params
    if (findParams(req.query, fields) !== true) {
        res.status(404);
        return res.json({ message: "Reqired fields", error: fields });
    }

    //set detault limit
    var limit = req.query.limit || 10;
    //get weather based on city
    searchCities(req.query.search, limit).then((data) => {
        var tempInfo = typeof data == 'object' ? data : JSON.parse(data);
        res.status(200)
        return res.json(tempInfo);
    }).catch((err) => {
        res.status(500);
        return res.json(err);
    })
})

const getWeatherDetails = (city) => {
    return new Promise((resolve, reject) => {
        //OAuth of yahoo API
        const appRequest = new OAuth.OAuth(null, null, yahoo_customerKey, yahoo_customerSecret, '1.0', null, 'HMAC-SHA1', null, { "X-Yahoo-App-Id": yahoo_appId });

        //Cross reference city to yahoo location and format json
        var query = "?location=" + city + "&format=json";
        var url = yahoo_api_url + query
        //set request to yahoo API
        appRequest.get(url, null, null, (err, data, res) => {
            if (res.statusCode == 200)
                return resolve(data);
            return reject({ error: err, message: "Internal Server Error" });
        })
    })
}

const searchCities = (city, limit) => {
    return new Promise((resolve, reject) => {
        //OAuth of yahoo API
        const appRequest = new OAuth.OAuth(null, null, null, null, '1.0', null, 'HMAC-SHA1', null, null);

        //Cross reference city to yahoo location and format json
        var query = "?search=" + city + "&limit=" + limit;
        var url = teleport_api_url + query;
        //set request to yahoo API
        appRequest.get(url, null, null, (err, data, res) => {
            if (res.statusCode == 200) {
                let cities = filterCityNames(data);
                return resolve(cities);
            }
            return reject({ error: err, message: "Internal Server Error" });
        })
    })
}

const filterCityNames = (data) => {
    try {
        let cities = typeof data == 'object' ? data : JSON.parse(data);;
        if (cities && cities._embedded) {
            let search_res = cities._embedded["city:search-results"];
            if (Array.isArray(search_res) && search_res.length > 0) {
                return search_res.map((c) => {
                    return c.matching_full_name;
                }).filter(x => x);
            }
        }
    } catch (err) {
        return [];
    }

}
const findParams = (params, fields) => {
    if (!params || typeof params != 'object' || Object.keys(params).length == 0) return;
    for (var param in params) {
        if (fields.includes(param) && params[param] !== null) return true;
    }
    return false;
}

app.listen(port, () => console.log(`Example app listening on port ${port}!`))