# Weather API

## Weather API based on Yahoo Weather API and Teleport API



#### Get list of cities


*`GET:`* `{{base_path}}/cities`

*query params*: `search`, `limit`

*Example:* `http://localhost:4545/search?search=Da&limit=5`

*Response*
```json
[
    "Dallas, Texas, United States",
    "Dallas, Oregon, United States",
    "Dallas, Georgia, United States",
    "Dallas, North Carolina, United States",
    "Dallastown, Pennsylvania, United States",
    "Dallas, Pennsylvania, United States",
    "Dallas Center, Iowa, United States",
    "Bethany, Missouri, United States (Dallas)",
    "Marble Hill, Missouri, United States (Dallas)",
    "Port Norris, New Jersey, United States (Dallas Ferry)"
]
```


#### Get the weather information based on the city


*`GET:`* `{{base_path}}/weather`

*query params*: `city`

*Example:* `http://localhost:4545/weather?city=Dallas, Texas, United States`

*Response*
```json
{
    "location": {
        "city": "Dallas",
        "region": " TX",
        "woeid": 2388929,
        "country": "United States",
        "lat": 32.78183,
        "long": -96.79586,
        "timezone_id": "America/Chicago"
    },
    "current_observation": {
        "wind": {
            "chill": 46,
            "direction": 30,
            "speed": 13.67
        },
        "atmosphere": {
            "humidity": 77,
            "visibility": 10,
            "pressure": 29.59,
            "rising": 0
        },
        "astronomy": {
            "sunrise": "7:29 am",
            "sunset": "5:48 pm"
        },
        "condition": {
            "text": "Scattered Showers",
            "code": 39,
            "temperature": 51
        },
        "pubDate": 1611057600
    },
    "forecasts": [
        {
            "day": "Tue",
            "date": 1611036000,
            "low": 51,
            "high": 60,
            "text": "Showers",
            "code": 11
        },
        {
            "day": "Wed",
            "date": 1611122400,
            "low": 47,
            "high": 52,
            "text": "Scattered Showers",
            "code": 39
        },
        {
            "day": "Thu",
            "date": 1611208800,
            "low": 51,
            "high": 66,
            "text": "Rain",
            "code": 12
        },
        {
            "day": "Fri",
            "date": 1611295200,
            "low": 54,
            "high": 64,
            "text": "Mostly Cloudy",
            "code": 28
        },
        {
            "day": "Sat",
            "date": 1611381600,
            "low": 49,
            "high": 59,
            "text": "Scattered Showers",
            "code": 39
        },
        {
            "day": "Sun",
            "date": 1611468000,
            "low": 55,
            "high": 70,
            "text": "Thunderstorms",
            "code": 4
        },
        {
            "day": "Mon",
            "date": 1611554400,
            "low": 52,
            "high": 68,
            "text": "Thunderstorms",
            "code": 4
        },
        {
            "day": "Tue",
            "date": 1611640800,
            "low": 46,
            "high": 63,
            "text": "Mostly Sunny",
            "code": 34
        },
        {
            "day": "Wed",
            "date": 1611727200,
            "low": 47,
            "high": 61,
            "text": "Scattered Showers",
            "code": 39
        },
        {
            "day": "Thu",
            "date": 1611813600,
            "low": 43,
            "high": 60,
            "text": "Partly Cloudy",
            "code": 30
        }
    ]
}
```